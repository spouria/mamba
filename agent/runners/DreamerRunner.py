import ray
import wandb
import torch
from agent.workers.DreamerWorker import DreamerWorker


class DreamerServer:
    def __init__(self, n_workers, env_config, controller_config, model):
        ray.init()

        self.workers = [
            DreamerWorker.remote(i, env_config, controller_config)
            for i in range(n_workers)
        ]
        self.tasks = [worker.run.remote(model) for worker in self.workers]

    def append(self, idx, update):
        self.tasks.append(self.workers[idx].run.remote(update))

    def run(self):
        done_id, tasks = ray.wait(self.tasks)
        self.tasks = tasks
        recvs = ray.get(done_id)[0]
        return recvs


class DreamerRunner:
    def __init__(
        self, env_config, learner_config, controller_config, n_workers, saved_model=None
    ):
        self.n_workers = n_workers
        self.learner = learner_config.create_learner(saved_model=saved_model)
        self.server = DreamerServer(
            n_workers, env_config, controller_config, self.learner.params()
        )
        self.saved_model = saved_model

    def run(self, max_steps=10**10, max_episodes=10**10, save_checkpoint=False):
        cur_steps, cur_episode = 0, 0

        if self.saved_model:
            try:
                cur_episode = int(self.saved_model[5:9])
                cur_steps = int(self.saved_model[10:16])
                print(f"starting from episode {cur_episode} and step {cur_steps}")
            except Exception:
                print("could not retrieve name")

        wandb.define_metric("steps")
        wandb.define_metric("reward", step_metric="steps")

        while True:
            rollout, info = self.server.run()
            self.learner.step(rollout)
            cur_steps += info["steps_done"]
            cur_episode += 1
            wandb.log({"reward": info["reward"], "steps": cur_steps}, step=cur_steps)

            print(cur_episode, cur_steps, self.learner.total_samples, info["reward"])
            if save_checkpoint and cur_episode % 500 == 0:
                torch.save(
                    self.learner.model,
                    "saves/save_{:04d}_{:06d}".format(cur_episode, cur_steps),
                )
            if cur_episode >= max_episodes or cur_steps >= max_steps:
                break
            self.server.append(info["idx"], self.learner.params())

        wandb.finish()
